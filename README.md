INSTALLATION:
--------------------

    sudo ln -s {path/to/ssh-grep.sh} /usr/local/bin/ssh-grep

USAGE:
--------------------

    go to ansible-repositories/{project-name}
    ssh-grep prod grep ERROR /var/log/gp/locus-casino_django.log
