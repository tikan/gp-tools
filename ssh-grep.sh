#!/bin/bash


SSH_USER=${SSH_USER-"gpuser"}
RESULTS_DIR=${RESULTS_DIR-"$HOME/projects/locus-casino/grep_results"}
FILENAME=${FILENAME-$(date +'%Y-%m-%d_%H-%M-%S')}

env=$1; shift;
hosts=$(cat hosts* 2>/dev/null | grep -A64 -w "\[${env}\]" | grep -v "\[${env}\]" | awk '{print $1; if($0==""){exit 0;}}' | tr "\n" " ")

if [ -z "$hosts" ]; then
    echo "no hosts found for env=$env";
    exit 1;
fi

mkdir -p $RESULTS_DIR
fullpath="${RESULTS_DIR}/$FILENAME"

command="$@"

echo $command >/dev/stderr;
echo "Filename: $fullpath"

for host in ${hosts}; do
    echo "[$host]" >/dev/stderr;
    ssh "$SSH_USER"@"$host" -o ConnectTimeout=5 $command 2> >(grep -v Ubuntu) | tee -a "$fullpath"
done

echo "Filename: $fullpath"
