#!/usr/bin/env python

from __future__ import print_function
from collections import namedtuple
from os.path import expanduser

import sh
import os
import argparse

BASE_DIR = expanduser("~")
SSH_OTHER_DIR = '.ssh_other'
SSH_OTHER_PATH = os.path.join(BASE_DIR, SSH_OTHER_DIR)
SSH_CONFIG_FILE = os.path.join(BASE_DIR, '.ssh/config')
SSH_TIMEOUT = 2

ServerInfo = namedtuple("ServerName", "name ip")


def get_all_users():
    return sh.ls('-1', SSH_OTHER_PATH).splitlines()


def get_all_servers():
    with open(SSH_CONFIG_FILE) as ssh_config:
        lines = ssh_config.readlines()

    servers = [ServerInfo(name=lines[i].split()[1], ip=lines[i + 1].split()[1])
               for i, line in enumerate(lines)
               if 'Host ' in line]

    servers = [server for server in servers if '-vagrant' not in servers]

    return servers


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('user', nargs='?')
    parser.add_argument('servers', nargs="*")
    parser.add_argument('--ssh-user', default='gpuser')
    parser.add_argument('--prod', action='store_true', default=False)
    parser.add_argument('--dry-run', action='store_true', default=False)

    args = parser.parse_args()

    servers = get_all_servers()

    if args.servers:
        servers = [server for server in servers
                   if server.ip in args.servers or server.name in args.servers]
    else:
        if args.prod:
            servers = [server for server in servers if '-prod' in server.name]

    if args.user:
        users = [args.user]
    else:
        users = get_all_users()

    if not servers:
        print("No servers found.")

    ssh_copy_id = sh.Command('ssh-copy-id')
    for user in users:

        user_dir = os.path.join(SSH_OTHER_PATH, user)

        for server in servers:
            ssh_user_server = "{}@{}".format(args.ssh_user, server.ip)
            print("adding {} to {} ({})".format(user, ssh_user_server, server.name))
            if args.dry_run:
                continue
            try:
                ssh_copy_id('-o', 'ConnectTimeout={}'.format(SSH_TIMEOUT),
                            '-o' 'BatchMode=yes',
                            '-i', os.path.join(user_dir, 'id_rsa.pub'), ssh_user_server)
            except sh.ErrorReturnCode as e:
                print("error adding key: %s" % e)
