#!/usr/bin/env bash

SSHO=~/.ssh_other
PK=~/projects/ansible-repositories/gp_role_playbooks/gp_auth_keys/pubkeys/
mkdir -p ${SSHO}
for k in $(ls -1 $PK); do
    kf=$k@grandparade.co.uk
    mkdir -p ${SSHO}/$kf
    echo "Preparing $kf"
    cp ${PK}/$k ${SSHO}/$kf/id_rsa.pub
    touch ${SSHO}/$kf/id_rsa
done
